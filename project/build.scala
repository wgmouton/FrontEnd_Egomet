package deployScripts

import sbt.Keys._
import sbt._
import sbtrelease.{Git, Version}
import sbtrelease.ReleasePlugin.autoImport.ReleaseStep
import sbt.complete.Parser
import sbtrelease.ReleasePlugin.autoImport._
import sbtbuildjobs.BuildJobsPlugin.autoImport._

object Implicits {
  implicit def vcs(state: State): Git = sbtrelease.Git.mkVcs(Project.extract(state).get(baseDirectory))
}

object BuildAndDeployPlugin extends AutoPlugin {

  import autoImport._, Environment._, releaseSteps._


  object autoImport {
    val releaseEnvironment = AttributeKey[Environment]("Production, UAT, Dev")
    val dockerImages = AttributeKey[Seq[String]]("Production, UAT, Dev")
  }

  sealed trait Environment {
    def _releaseProcess: Seq[ReleaseStep]
  }

  object Environment {

    case object Production extends Environment {
      override def _releaseProcess: Seq[ReleaseStep] = Seq(
        _setEnvironment(this),
        _compile,
        releaseStepCommand("docker:stage"),
        //        _buildDockerImage("$DOCKER_IMAGE_REPO", _ ⇒ "latest"),
        _tagDockerImage("$DOCKER_IMAGE_REPO", _ ⇒ "latest", "$DOCKER_IMAGE_REPO", v ⇒ v),
        //        _testDockerImage,
        _deployDockerImage
      )
    }

    case object UAT extends Environment {
      override def _releaseProcess: Seq[ReleaseStep] = Seq(
        _setEnvironment(this),
        _compile,
        _updateVersion,
        releaseStepCommand("docker:stage"),
        //        _buildDockerImage("$DOCKER_IMAGE_REPO", _ ⇒ "uat"),
        //        _testDockerImage,
        _deployDockerImage
      )
    }

    case object Dev extends Environment {
      override def _releaseProcess: Seq[ReleaseStep] = Seq(
        _setEnvironment(this),
        _updateBuildInfo,
        releaseStepCommand("docker:stage"),
        _buildDockerImage("egomet", _ ⇒ "dev", "target/docker/stage"),
        _pushDockerImage
      )
    }

  }

  object releaseSteps {
    //Step 1: Ready environment
    def _setEnvironment(env: Environment): ReleaseStep = ReleaseStep { st: State ⇒
      st.put(releaseEnvironment, env)
    }


    //Set 2: Compile
    lazy val _compile: ReleaseStep = ReleaseStep { st: State =>
      println("compile")
      val extracted = Project.extract(st)
      val ref = extracted.get(thisProjectRef)
      extracted.runAggregated(compile in Global in ref, st)
      //    println(st.get(releaseEnvironment).toString)
      st
    }


    lazy val _updateVersion: ReleaseStep = ReleaseStep { st: State ⇒
      val extracted = Project.extract(st)

      def updateVersion(v: Option[String]): State = {
        v.map(versionString ⇒ extracted.append(Seq(version := versionString), st))
          .getOrElse(sbtrelease.versionFormatError)
      }

      st.get(releaseEnvironment) match {
        case Some(Dev) ⇒ updateVersion(Version(extracted.get(version)).map(_.bumpBugfix.string))
        case Some(UAT) ⇒ updateVersion(Version(extracted.get(version)).map(_.bumpMinor.string))
        case _ ⇒ st
      }
    }

    lazy val _updateBuildInfo: ReleaseStep = ReleaseStep { st: State ⇒
      val extracted = Project.extract(st)

      def setState(_version: String, _buildNumber: Int, _commitSHA: String) = {
        IO.writeLines(st.baseDir / "buildinfo.sbt", Seq(s"""version := "${_version}"""", s"buildNumber := ${_buildNumber}", s"""commitSHA := "${_commitSHA}""""))
        extracted.append(Seq(version := _version, buildNumber := _buildNumber, commitSHA := _commitSHA), st)
      }

      val cVersion: (Version ⇒ Version) ⇒ String = (v: Version ⇒ Version) ⇒
        Version(extracted.get(version)).map(v(_).string).getOrElse(sbtrelease.versionFormatError)

      val cBuildNumber: Int = extracted.getOpt(buildNumber).getOrElse(0)
      val cCommitSHA: String = extracted.getOpt(baseDirectory).map(Git.mkVcs(_).currentHash).getOrElse("Unknown SHA")

      st.get(releaseEnvironment) match {
        case Some(Dev) ⇒ setState(
          _version = cVersion(_.bumpBugfix),
          _buildNumber = cBuildNumber + 1,
          _commitSHA = cCommitSHA
        )
        case Some(UAT) ⇒ setState(
          _version = cVersion(_.bumpMinor),
          _buildNumber = cBuildNumber + 1,
          _commitSHA = cCommitSHA
        )
        case _ ⇒ st
      }

      st
    }

    lazy val _pushVersionAndTag: ReleaseStep = ReleaseStep { st: State ⇒ st }

    lazy val _setInfo: ReleaseStep = ReleaseStep { st: State ⇒
      //    val globalVersionString = "version in ThisBuild := \"%s\"" format versionString
      //Write to file
      //        IO.writeLines(file, Seq(globalVersionString))
      st
    }

    // Build docker images
    def _buildDockerImage(imageName: String, tag: String ⇒ String, file: String): ReleaseStep = ReleaseStep { st: State ⇒
      val extracted = Project.extract(st)

      val image = s"$imageName:${tag(extracted.getOpt(version) getOrElse "latest")}"

      s"docker build -t $image ${(st.baseDir / file).getAbsolutePath}" !

      st.put(dockerImages, st.get(dockerImages).fold(Seq(image))(images ⇒ if (images.contains(image)) images else images ++ Seq(image)))
    }

    lazy val _testDockerImage: ReleaseStep = ReleaseStep { st: State ⇒

      st
    }


    def _tagDockerImage(imageName: String, tag: String ⇒ String, imageNameNew: String, tagNew: String ⇒ String): ReleaseStep = ReleaseStep { st: State ⇒
      val extracted = Project.extract(st)

      //      val versionTag = extracted.getOpt(version) getOrElse "latest"
      //      val image = s"$imageName:${tag(versionTag)}"
      //      val imageNew = s"$imageNameNew:${tagNew(versionTag)}"
      //
      //      val images = st.get(dockerImages) match {
      //        case optImages if optImages.exists(!_.contains(image)) ⇒ sys.error("Base image does not exist")
      //        case Some(images) if images.contains(imageNew) ⇒ sys.error("New Image already exists")
      //        case Some(images) ⇒ images ++ Seq(imageNew)
      //      }
      //      st.put(dockerImages, images)
      st
    }

    lazy val _pushDockerImage: ReleaseStep = ReleaseStep { st: State ⇒
      for (x ← st.get(dockerImages); image ← x) (s"docker push $image" !)
      st
    }

    lazy val _deployDockerImage: ReleaseStep = ReleaseStep { st: State ⇒ st }
  }


  //  def terminal(command: String) = sbt.Process("docker") !
}
