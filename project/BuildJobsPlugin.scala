package sbtbuildjobs

import sbt._
import Keys._
import _root_.sbt.complete.DefaultParsers._
import sbt.complete.DefaultParsers._
import sbt.complete.Parser

// imports standard command parsing functionality

object BuildJobsPlugin extends AutoPlugin {

  import autoImport._, BuildJobsKeys._

  type Environment = String

  case class Version(major: Int, minors: List[Int], versionSuffix: Option[(String, Option[Int])], metaData: Option[String]) {

    def increment(i: Int): Int = i + 1

    def incMajor = this.copy(major + 1, minors = minors.map(_ ⇒ 0))

    def incMinor = incMinors(0)

    def incPatch = incMinors(1)

    def incNano = incMinors(2)

    def incMinors(index: Int) = {
      val ver = minors ++ List.fill(index - minors.length + 1)(0)
      this.copy(minors = ver.zipWithIndex.foldRight(List.empty[Int]) {
        case ((v, i), nl) if i < index ⇒ v +: nl
        case ((v, i), nl) if i == index ⇒ (v + 1) +: nl
        case ((v, i), nl) if i > index ⇒ 0 +: nl
        case (_, nl) ⇒ nl
      })
    }

    def incSuffix = versionSuffix match {
      case Some((s, Some(sv))) ⇒ this.copy(versionSuffix = Some(s → Some(sv + 1)))
      case Some((s, None)) ⇒ this.copy(versionSuffix = Some(s → Some(1)))
      case None ⇒ this
    }

    def addVersionSuffix(suffix: String, suffixVersion: Option[Int]) = this.copy(versionSuffix = Some(suffix → suffixVersion))

    def removeSuffix = this.copy(versionSuffix = None)

    def addMetaData(metaString: String*) = this.copy(metaData = if (metaString.nonEmpty) Some(metaString.mkString(".")) else None)

    def removeMetaData = this.copy(metaData = None)

    override def toString: String = {
      val versionSuffix = this.versionSuffix.fold("") { case (s, sv) ⇒ "-" + s + sv.fold("")("." + _) }
      val versionMetaData = this.metaData.fold("")("+" + _)
      s"""$major${minors.mkString(".", ".", "")}$versionSuffix$versionMetaData"""
    }
  }

  val versionRegex = "([0-9]+)+(\\.[0-9]+)*(\\-[0-9a-zA-Z]+(\\.[0-9]+)?)?(\\+[0-9a-zA-Z]+(\\.[0-9a-zA-Z]+)*)?"

  object Version {
    def apply(version: String): Version = mkVersion(version)

    def fromState(state: State) = Project.extract(state).getOpt(version).map(mkVersion).getOrElse(sys.error("version not found"))

    def mkVersion(version: String) = if (version.matches(versionRegex)) {
      val v = "([0-9]+)+(\\.[0-9]+)*".r.findFirstIn(version).map(_.split("\\.").toList.map(_.toInt))
      val s = "\\-[0-9a-zA-Z]+(\\.[0-9]+)?".r.findFirstIn(version).map(_.tail.split("\\.").toList)
      val m = "\\+[0-9a-zA-Z]+(\\.[0-9a-zA-Z]+)*".r.findFirstIn(version).map(_.tail)

      v match {
        case Some(vh :: vt) ⇒ Version(vh, vt, s.flatMap { case (sh :: st) ⇒ Some(sh → st.headOption.map(_.toInt)) case _ ⇒ None }, m)
        case None ⇒ sys.error("Invalid version")
      }
    } else sys.error("Invalid version format. (Check that your version complies the the semantic versioning standards.)")
  }

  object autoImport {

    case class Job(name: String, script: (Environment, State) ⇒ String) {
      def mkFile(currentEnv: String): State ⇒ State = { (st: State) ⇒
        genFile(st.baseDir / "target" / "jobs" / name)("#!/usr/bin/env bash", script(currentEnv, st))
        st
      }
    }

    lazy val buildState = SettingKey[(Environment, State) ⇒ State]("buildState")
    lazy val buildJobs = SettingKey[Seq[Job]]("buildJobs")
    lazy val buildEnvironment = SettingKey[Map[String, Seq[String]]]("")
    lazy val buildNumber = SettingKey[Int]("buildNumber", "Build number of the project")
    lazy val buildMetaData = SettingKey[Map[String, Any]]("buildMetaData")
    lazy val commitSHA = SettingKey[String]("commitSha", "Commit SHA")

  }

  object BuildJobsKeys {
    private val FailureCommand = "--failure--"

    private lazy val releaseCommandKey = "prepareJobs"

    private[this] val environment: Parser[ParseResult] =
      (Space ~> token("env") ~> Space ~> token(StringBasic, "<environment>")) map ParseResult.Environment

    private[this] sealed abstract class ParseResult extends Product with Serializable

    private[this] object ParseResult {

      final case class Environment(value: String) extends ParseResult

    }

    val prepareJobsCommand: Command = Command(releaseCommandKey)(_ ⇒ environment.*) { (st, args) ⇒
      val extracted = Project.extract(st)

      val startState = st
        .copy(onFailure = Some(FailureCommand))

      val env = args collectFirst { case ParseResult.Environment(value) ⇒ value } getOrElse "none"
      val jobs = extracted.getOpt(buildJobs).getOrElse(Seq())
      val buildSt = extracted.getOpt(buildState).map(_ (env, startState)) getOrElse startState

      val newJobs = if (jobs.isEmpty) jobs
      else {
        jobs :+ Job(
          name = "release",
          script = { (_, _) ⇒
            jobs.foldRight("")((job, s) ⇒ s + s". ./${job.name}\n")
          }
        )
      }

      Function.chain(newJobs .map(_.mkFile(env)))(buildSt)
    }
  }

  def genFile(file: File)(content: String*) = {
    IO.write(file, content.mkString("\n\n"))
    if (file.isFile) s"chmod 555 $file" !
  }
}

object Jobs {

  import sbtbuildjobs.BuildJobsPlugin.autoImport.Job

  //  val compile = Job("compile") { st ⇒
  //    val extracted = Project.extract(st)
  //    "sbt compile"
  //  }
}