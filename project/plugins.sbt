logLevel := Level.Warn

resolvers += "Typesafe repository" at "https://dl.bintray.com/typesafe/maven-releases/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.4")

// Coffee script
addSbtPlugin("com.typesafe.sbt" % "sbt-coffeescript" % "1.0.0")

// Sass
addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.6")

// Deployment
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.3")
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.6.1")