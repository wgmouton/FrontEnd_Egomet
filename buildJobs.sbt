lazy val dockerLogin = s"""docker login ${sys.env.getOrElse("REGISTRY_URL", "")} -u $$REGISTRY_USERNAME -p $$REGISTRY_PASSWORD"""

buildState := { (env, st) ⇒
  import sbtbuildjobs.BuildJobsPlugin._

  val v = Version.fromState(st)
  val bn = Project.extract(st).getOpt(buildNumber).map(_ + 1).getOrElse(sys.error("Build number not found"))
  val hash = sbtrelease.Git.mkVcs(st.baseDir).currentHash

  val newVersion = env match {
    case "prod" ⇒ v.removeSuffix.removeMetaData

    case "release" ⇒ v match {
      case Version(_, _, Some(("rc", _)), _) ⇒ v.incSuffix.removeMetaData
      case _ ⇒ v.incMinor.addVersionSuffix("rc", Some(1)).removeMetaData
    }

    case "dev" ⇒ v.removeSuffix.addMetaData(bn.toString)
  }

  genFile(st.baseDir / "buildinfo.sbt") {
    s"""
       |version := "$v"
       |buildNumber := $bn
       |commitSHA := "$hash"
    """.stripMargin
  }

  Project.extract(st).append(Seq(version := newVersion.toString, buildNumber := bn, commitSHA := hash), st)
}

buildJobs := Seq(

  Job(
    name = "compile",
    script = {
      case (_, st) ⇒
        """
          |sbt compile
          |sbt docker:stage
        """.stripMargin
    }
  ),

  Job(
    name = "test",
    script = {
      case (_, st) ⇒
        "sbt test"
    }
  ),

  Job(
    name = "push_image",
    script = { (env, st) ⇒
      val extracted = Project.extract(st)
      val currentVersion = extracted.getOpt(version).getOrElse(sys.error("Version not found"))
      val dockerFile = extracted.getOpt(target).map(_ / "docker" / "stage").getOrElse(sys.error("Dockerfile not found"))

      s"""
         |$dockerLogin
         |docker build -t $$IMAGE_REGISTRY:build $dockerFile\n
         |docker tag $$IMAGE_REGISTRY:build $$IMAGE_REGISTRY:$currentVersion
         |docker push $$IMAGE_REGISTRY:$currentVersion
        """.stripMargin
    }
  ),

  Job(
    name = "push_version",
    script = { (env, st) ⇒
      val currentVersion = Project.extract(st).getOpt(version).getOrElse(sys.error("Version not found"))

      s"""
         |git commit -m "Updated project version to $currentVersion [ci skip]" buildinfo.sbt
      """.stripMargin + (env match {
        case "prod" ⇒
          s"""
             |git push origin $$(git rev-parse HEAD):master
          """.stripMargin
        case "release" ⇒
          s"""
             |git push origin $$(git rev-parse HEAD):release
          """.stripMargin
        case "dev" ⇒
          s"""
             |git push origin $$(git rev-parse HEAD):develop
          """.stripMargin
      })
    }
  ),

  Job(
    name = "deploy",
    script = { (env, st) ⇒
      val currentVersion = Project.extract(st).getOpt(version).getOrElse(sys.error("Version not found"))

      s"""
         |$dockerLogin
         |docker pull $$IMAGE_REGISTRY:$currentVersion
         |
        """.stripMargin + (env match {
        case "prod" ⇒
          s"""
             |docker tag $$IMAGE_REGISTRY:$currentVersion $$IMAGE_REGISTRY:latest
             |docker push $$IMAGE_REGISTRY:latest
             |curl -X POST "$$DEPLOY_URL_PROD"
          """.stripMargin

        case "uat" ⇒
          s"""
             |docker tag $$IMAGE_REGISTRY:$currentVersion $$IMAGE_REGISTRY:latest
             |docker push $$IMAGE_REGISTRY:uat
             |curl -X POST "$$DEPLOY_URL_UAT"
          """.stripMargin

        case "dev" ⇒
          s"""
             |docker tag $$IMAGE_REGISTRY:$currentVersion $$IMAGE_REGISTRY:latest
             |docker push $$IMAGE_REGISTRY:dev
          """.stripMargin
      })
    }
  )
)