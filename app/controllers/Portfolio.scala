package controllers

import play.api.mvc._
import helpers.CustomAction._
import views.html._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by WG on 2016/09/21.
  */
class Portfolio extends Controller {
  def frame() = StatAction { implicit request ⇒
    Ok(frames.Portfolio())
  }
}
