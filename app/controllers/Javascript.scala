package controllers

import play.api.mvc._
import play.api.routing._

/**
  * Created by WG on 2016/03/24.
  */
class Javascript extends Controller {
  val JsRoutes = List(
//    controllers.routes.javascript.Introduction.body
  )

  def routes = Action(implicit request =>
    Ok(JavaScriptReverseRouter("jsRoutes")(JsRoutes: _*)).as("text/javascript")
  )
}
