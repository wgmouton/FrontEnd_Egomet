package controllers

import play.api.mvc._
import play.api.routing._
import play.twirl.api.Html
import views.html._
import helpers._
import Conversions._

import scalaz._
import Scalaz._
import scala.util.Try
import org.wgmouton.common._
import definitions._
import Implicits._
import CustomAction._

class Application extends Controller {


  def about() = Action(Ok(frames.Status(meta.BuildInfo.toMap)))

  def health() = Action(Ok("Healthy"))

  def index() = Action {
    import scalaz._, Scalaz._

    val state = State.init[List[String]]

    state.flatMap(_ ⇒ State[List[String], Unit] { st ⇒ (st ::: List("Hi"), ()) })
    state.flatMap(_ ⇒ State[List[String], Unit] { st ⇒ (st ::: List("Nye"), ()) })
    state.flatMap(_ ⇒ State[List[String], Unit] { st ⇒ (st ::: List("Nye"), ()) })

    state.map(s ⇒ State.get)
    println(state(List()))

    //    frames.Portfolio(
    //      pages.Introduction()
    //    )
    Ok
  }

  def test() = Action {
    -\/-("")
    print(Configuration.Application(_.getString("session.name")))
    //    Ok(frames.Portfolio(
    //      pages.Introduction()
    //    ))
    Ok
  } //AuthAction(analytics = false) {
  // ""
  //}

  def postTest() = Action { request ⇒

    println(request.headers.headers.mkString("\n"))
    println(request.body.asText.getOrElse("No Post"))
    Ok(request.body.asText.getOrElse("No Post"))
  }

  def linkedIn() = StatAction(Redirect("https://za.linkedin.com/in/wgmouton"))

  def gitHub() = StatAction(Redirect("https://github.com/wgmouton"))

  def gitLab() = StatAction(Redirect("https://gitlab.com/u/wgmouton"))

  def bitBucket() = StatAction(Redirect("https://bitbucket.org/wgmouton"))

  def facebook() = StatAction(Redirect("https://www.facebook.com/wg.mouton"))

  def twitter() = StatAction(Redirect("https://twitter.com/WG_Mouton"))

  def googlePlus() = StatAction(Redirect("/linkedIn"))

}