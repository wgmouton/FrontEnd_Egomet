$ =>
  $(".page-container").fullpage
    menu: '#menu'
    lockAnchors: false
    anchors: [
      'introduction'
      'projects'
      'find-me'
    ]
    navigation: false
    navigationPosition: 'bottom'
    navigationTooltips: []
    showActiveTooltip: false
    slidesNavigation: false
    slidesNavPosition: 'bottom'
    css3: true
    scrollingSpeed: 700
    autoScrolling: true
    fitToSection: true
    fitToSectionDelay: 1000
    scrollBar: false
    easing: 'easeInOutCubic'
    easingcss3: 'ease'
    loopBottom: false
    loopTop: false
    loopHorizontal: false
    continuousVertical: false
    continuousHorizontal: false
    scrollHorizontally: false
    interlockedSlides: false
    resetSliders: false
    fadingEffect: false
    normalScrollElements: '#element1, .element2'
    scrollOverflow: true
    scrollOverflowOptions:
      click: true
    touchSensitivity: 5
    normalScrollElementTouchThreshold: 3
    bigSectionsDestination: null
    keyboardScrolling: true
    animateAnchor: true
    recordHistory: false
    controlArrows: false
    verticalCentered: false
#    sectionsColor: [
#      '#ccc'
#      '#fff'
#    ]
#    paddingTop: '3em'
#    paddingBottom: '10px'
    fixedElements: '#frame-nav'
    responsiveWidth: 0
    responsiveHeight: 0
    responsiveSlides: false
    sectionSelector: '.section'
    slideSelector: '.slide'
    onLeave: (index, nextIndex, direction) ->
    afterLoad: (anchorLink, index) ->
      $("#frame-nav > ul").removeClass("nav-white")
      $("#social-media-container > ul").removeClass("nav-white")
      $("#social-media-container").removeClass("hidden")
      switch anchorLink
        when "projects"
          $("#frame-nav > ul").addClass("nav-white")
          $("#social-media-container > ul").addClass("nav-white")
        when "find-me"
          $("#social-media-container").addClass("hidden")

    afterRender: ->
    afterResize: ->
    afterSlideLoad: (anchorLink, index, slideAnchor, slideIndex) ->
    onSlideLeave: (anchorLink, index, slideIndex, direction, nextSlideIndex) ->

  $(".page-container").fullpage.reBuild()

$('#pagepiling').pagepiling
  menu: null
  direction: 'vertical'
  verticalCentered: true
  sectionsColor: []
  anchors: []
  scrollingSpeed: 700
  easing: 'swing'
  loopBottom: false
  loopTop: false
  css3: true
  navigation:
    'textColor': '#000'
    'bulletsColor': '#000'
    'position': 'right'
    'tooltips': [
      'section1'
      'section2'
      'section3'
      'section4'
    ]
  normalScrollElements: null
  normalScrollElementTouchThreshold: 5
  touchSensitivity: 5
  keyboardScrolling: true
  sectionSelector: '.section'
  animateAnchor: false
  onLeave: (index, nextIndex, direction) ->
  afterLoad: (anchorLink, index) ->
  afterRender: ->