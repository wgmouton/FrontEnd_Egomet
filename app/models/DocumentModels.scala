package models

/**
  * Created by WG on 2016/09/26.
  */

case class Skill(title: String, description: Option[String], proficicy: Int)

case class Experience(title: String, date: String, description: String)