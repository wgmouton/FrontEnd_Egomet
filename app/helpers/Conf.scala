package helpers

import com.typesafe.config.{Config, ConfigFactory}
import scalaz.\/
import org.wgmouton.common.definitions._

/**
  * Created by WG on 2016/04/14.
  */
sealed trait BaseConfiguration {
  def config: Config

  def apply[A](f: ⇒ Config ⇒ A): Throwable \/ A = -\/-(f(config))
}

object Configuration {
  def apply[A](config: String)(f: ⇒ Config ⇒ A) = -\/-(f(ConfigFactory.load(config)))

  object Application extends BaseConfiguration {
    override def config = ConfigFactory.load()
  }

  object Assets extends BaseConfiguration {
    override def config = ConfigFactory.load("assets")
  }

}
