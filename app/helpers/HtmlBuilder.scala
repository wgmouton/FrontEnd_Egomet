package helpers

import play.twirl.api._
import play.api.mvc.Call

import scala.language.implicitConversions
import org.wgmouton.common.definitions._

/**
  * Created by WG on 2016/04/15.
  */

object AssetBuilder {

  import HtmlBuilder._

  sealed trait AssetType

  object JS extends AssetType

  object CSS extends AssetType

  object Asset {
    def apply(assetType: AssetType, url: String): Html = assetType match {
      case JS ⇒ importJavascript(url)
      case CSS ⇒ importStylesheet(url)
    }

    def apply(assetType: AssetType, call: Call): Option[Html] = None

//    def apply(assetType: AssetType, path: String): Option[Html] = Configuration.Application(_.getString("state")).flatMap(state ⇒ assetType match {
//      case JS ⇒ Configuration.Assets(_.getConfig(s"$state.js").getString(path))
//      case CSS ⇒ Configuration.Assets(_.getConfig(s"$state.css").getString(path))
//    })
  }

}

object HtmlBuilder {


  implicit class StringInterpolation(val sc: StringContext) extends AnyVal {

    def html(args: Any*): Html = interpolate(args, HtmlFormat)

    def xml(args: Any*): Xml = interpolate(args, XmlFormat)

    def js(args: Any*): JavaScript = interpolate(args, JavaScriptFormat)

    def interpolate[A <: Appendable[A] : Manifest](args: Seq[Any], format: Format[A]): A = {
      sc.checkLengths(args)
      val array = Array.ofDim[Any](args.size + sc.parts.size)
      val strings = sc.parts.iterator
      val expressions = args.iterator
      array(0) = format.raw(strings.next())
      var i = 1
      while (strings.hasNext) {
        array(i) = expressions.next()
        array(i + 1) = format.raw(strings.next())
        i += 2
      }
      new BaseScalaTemplate[A, Format[A]](format)._display_(array)
    }

  }

  def importJavascript(url: String): Html = html"<script src='$url'></script>"

  def importStylesheet(url: String): Html = html"<link rel='stylesheet' href='$url'>"

}
