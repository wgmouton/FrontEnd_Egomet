package helpers

import play.api.{Logger, Play}
import play.api.data.{Form, FormError, Mapping}
import play.api.mvc.{Action, _}
import play.api.mvc.Results._
import scala.language.higherKinds

import scala.concurrent.{ExecutionContext, Future}
import scala.language._

/**
  * Created by WG on 2016/04/06.
  */
case class User(id: String, username: String, firstname: String, lastname: String)

class AuthenticatedRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)

object CustomAction {

  object ActionFunctions {

    object CheckSSLAction extends ActionBuilder[Request] with ActionFilter[Request] {
      def filter[A](request: Request[A]): Future[Option[Result]] = Future.successful {
        request.headers.get("X-Forwarded-Proto") match {
          case Some(proto) if proto == "https" => None
          case _ => Some(Results.Forbidden)
        }
      }
    }

    object AuthenticateAction extends ActionBuilder[AuthenticatedRequest] with ActionRefiner[Request, AuthenticatedRequest] {
      def unauthorized: Result = Redirect("/login").withNewSession

      def authenticate: Future[Option[User]] = {
        Future(Option[User](User("id", "username", "firstname", "lastname")))(executionContext)
        //      Future(None)(executionContext)
      }

      def refine[A](input: Request[A]) = authenticate.map(_.map(user ⇒
        new AuthenticatedRequest(user, input)
      ).toRight(unauthorized))(executionContext)
    }

  }

  import ActionFunctions._

  object StatAction extends ActionBuilder[Request] {
    override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
      Logger.info("Stats happend")
      block(request)
    }
  }

  object CheckRequestOrigin extends ActionBuilder[Request] with ActionFilter[Request] {
    def filter[A](request: Request[A]): Future[Option[Result]] = Future.successful {
      request.headers.get("X-Forwarded-Proto") match {
        case Some(proto) if proto == "https" => None
        case _ => Some(Results.Forbidden)
      }
    }
  }

  def AuthAction = /*CheckSSLAction andThen */AuthenticateAction
}

/*
def refine[A](input: Request[A]) = Future.successful {
  //      Form(fMap).bindFromRequest()(input).fold(
  //        invalidForm ⇒ invalidForm.errors.foldRight(Map.empty[String, List[String]]) {
  //          case (FormError(key, messages, _), map) ⇒
  //            val mapKey = if (key.isEmpty) "global" else key
  //            map.get(mapKey).fold(map.updated(mapKey, messages.toList))(value ⇒ map.updated(mapKey, value ++ messages))
  //        },
  //        form ⇒ MyRequest("Hallo", input)
  //      )
  //
  //      Either.cond("test" == "test", MyRequest("Hallo", input), NotFound)
  //    }
 */