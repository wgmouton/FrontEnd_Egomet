package helpers

import scala.concurrent.Future, scala.concurrent.ExecutionContext.Implicits.global
import language._

/**
  * Created by WG on 2016/04/06.
  */
object Conversions {

  implicit def callToString(call: play.api.mvc.Call): String = call.url

  sealed trait AtoSomething[A] {
    def future: Future[A]
  }

  implicit def AToFuture[A](r: A): AtoSomething[A] = new AtoSomething[A] {
    override def future: Future[A] = Future(r)
  }
}

object StringInterpolation {

}
