import com.typesafe.sbt.packager.docker._
import BuildAndDeployPlugin.Environment

name := "FrontEnd_Egomet"

lazy val `frontend_egomet` = (project in file("."))
  .enablePlugins(PlayScala, JavaAppPackaging, BuildInfoPlugin).settings(
  routesImport += "helpers.RouteBinders._",
  commands += sbtbuildjobs.BuildJobsPlugin.BuildJobsKeys.prepareJobsCommand
)

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  "bintray-wgmouton-libraries" at "http://dl.bintray.com/wgmouton/libraries"
)

libraryDependencies ++= Seq(jdbc, cache, ws, specs2 % Test,
  "org.scalaz" %% "scalaz-core" % "7.2.0",
  "org.wgmouton" %% "library_common" % "0.1.1199289"
)


// Docker image
dockerBaseImage := "wgmouton/java:jre-8"
dockerExposedPorts := Seq(9000)
dockerCommands += Cmd("MAINTAINER", "wgmmouton")

////val ver = hello.BuildInfo.version

//SBT BuildInfo
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion, buildNumber, commitSHA, libraryDependencies)
buildInfoPackage := "meta"
buildInfoOptions += BuildInfoOption.ToJson
buildInfoOptions += BuildInfoOption.BuildTime

// Release process
releaseProcess := (sys.props.get("env") match {
  case Some("prod") ⇒ Environment.Production._releaseProcess
  case Some("uat") ⇒ Environment.UAT._releaseProcess
  case Some("dev") ⇒ Environment.Dev._releaseProcess
  case _ ⇒ Seq()
})